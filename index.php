<?php

require_once __DIR__.'/controller/front.php';
require_once __DIR__.'/controller/admin.php';
require_once __DIR__.'/controller/security.php';

$secure = new Security ([
	"POST" => [
		"email"=> FILTER_SANITIZE_EMAIL,
		"login"=> FILTER_SANITIZE_EMAIL,
		"password"=> FILTER_SANITIZE_EMAIL,
		"title"=> FILTER_SANITIZE_STRING,
		"slug" => FILTER_SANITIZE_STRING,
		"slugComment" => FILTER_SANITIZE_STRING,
		"status" => FILTER_SANITIZE_NUMBER_INT,
		"slugcontent" => FILTER_SANITIZE_STRING,
		"image" => FILTER_SANITIZE_STRING,
		"content" => FILTER_SANITIZE_STRING,
		"author" => FILTER_SANITIZE_STRING,
		]
]);

$uri = explode ( "/", $secure->uri);
$dir = explode("\\", __DIR__); 
$localPath = array_pop($dir);
$pointer = array_search($localPath, $uri);
$uri = array_slice($uri, $pointer+1);
$config = [
	"path"=> "projet-blog/", 
	"email"=> "s.mariejoseph@gmail.com",
	"emailServer"=> "contact@jeanforteroche.com" //à modifier avec le vrai nom de domaine
];


switch ($uri[0]) {
  case 'admin':
    $page = new Admin(array_slice($uri, 1));
    break;
  case '':
    header('Location: /'.$localPath.'/accueil',false);
    break;
	// tous les autres cas
  default:
  // var_dump($uri);
    $page = new Front($uri);
    break;
}

echo $page->html;

?>