-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 14 nov. 2019 à 20:45
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `blog`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(32) NOT NULL,
  `date` datetime NOT NULL,
  `titre` varchar(45) NOT NULL,
  `image` varchar(45) NOT NULL,
  `contenu` text NOT NULL,
  `publie` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `article`
--

INSERT INTO `article` (`id`, `slug`, `date`, `titre`, `image`, `contenu`, `publie`) VALUES
(1, 'article1', '2019-07-02 11:23:24', 'Chapitre 1 : L\'heure du choix', 'photochap1.jpg', 'Devant le bureau, ma lettre à la main, je sais qu\'il est encore temps de renoncer.\r\nJe peux continuer à me lever chaque matin pour ce job dans lequel je ne suis plus investie.\r\nNon, je ne reculerai. Ma démission en main, je frappe 3 coups et entre dans le bureau du DRH.\r\nIl ne s\'y attendait pas. \r\nMoi non plus… 4 mois avant, tout semblait suivre une route bien tracée.', 0),
(2, 'article2', '2019-07-02 11:23:24', 'Chapitre 2 : Le grand saut', 'photochap2.jpg', '3 mois me semblait être le temps nécessaire pour mettre en place mon projet. \r\nLes 3 mois sont passés si vite.\r\nQuand le taxi m\'a déposé à l\'aéroport tôt ce matin, je me rendais compte à quel point mon projet ressemblait à une mission suicide.\r\nMais je suis là, prête à monter dans l\'avion.\r\nPrête pour le grand saut.', 0),
(3, 'article3', '2019-07-03 11:23:24', 'Chapitre 3 : Turbulences', 'photochap3.jpg', 'L\'avion a décollé et j\'ai laissé derrière moi celle que j\'étais.\r\n\r\nEn plein vol, alors que mon voisin s\'est mis à ronflé, j\'ai sorti la lettre.\r\n\r\nLa fameuse lettre que je n\'avais pas eu le courage d\'ouvrir avant de partir.\r\n\r\n', 0),
(4, 'article4', '2019-07-04 11:23:24', 'Chapitre 4 : Alaska me voilà', 'photochap4.jpg', 'Bien arrivée. Enfin, je suis là, physiquement, mais je crois que mon esprit n\'est pas connecté.\r\n\r\nJe regarde tout autour de moi, c\'est l\'inconnu, je n\'ai plus mes repères.', 0),
(5, 'article5', '2019-10-30 21:15:35', 'Chapitre 5 : l\'aventure', 'photochap5.jpg', 'C\'était mon rêve de traverser le pays en traineau.\r\nMa rencontre avec John, guide, m\'a permis de le réaliser.\r\nAvec lui et sa horde de chiens loups, nous sommes partis à l\'aventure.', 0),
(6, 'article6', '2019-10-30 21:49:26', 'Chapitre 6 : la fin du brouillard', 'photochap6.jpg', '3 jours de marche, le froid pique mais la chaleur m\'envahit le cœur.\r\nC\'est la fin du brouillard, je sors de ma léthargie, je commence à y voir clair.', 0);

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

DROP TABLE IF EXISTS `commentaire`;
CREATE TABLE IF NOT EXISTS `commentaire` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `date` date NOT NULL,
  `author` varchar(100) NOT NULL,
  `slug_article` varchar(32) NOT NULL,
  `slug` varchar(23) NOT NULL,
  `status` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `commentaire`
--

INSERT INTO `commentaire` (`ID`, `title`, `content`, `date`, `author`, `slug_article`, `slug`, `status`) VALUES
(1, 'super', 'Bonne intro, hâte de découvrir la suite.', '2019-07-01', 'stéphanie', 'article1', 'c1', 3),
(2, 'bof', 'article plutôt succinct et inintéressant ', '2019-07-01', 'christophe', 'article1', 'c2', 2),
(3, 'top', 'hypra méga super article', '2019-07-01', 'gabi', 'article1', 'c3', 3),
(9, '1 étoile', 'bon début', '2019-09-22', 'critique de blog', 'article1', '5d87d62a3aeeb', 0),
(8, '1 étoile', 'comment test', '2019-09-22', 'critique de blog', 'article1', '5d87cfaa89153', 0),
(10, '2 étoiles', 'Test Test', '2019-09-24', 'Stéphanie', 'article1', '5d89ff1336bfd', 0),
(11, '1 étoile', 'test commentaire modération', '2019-10-03', 'critique de blog', 'article1', '15701035465d95e0fa81771', 3),
(12, '', 'test', '2019-11-06', '', 'article6', '15730756835dc33ae3dbab5', 1),
(13, 'hhhh', 'jjjj', '2019-11-14', 'jjj', 'article6', '15737359335dcd4dfd84e7d', 4);

-- --------------------------------------------------------

--
-- Structure de la table `email`
--

DROP TABLE IF EXISTS `email`;
CREATE TABLE IF NOT EXISTS `email` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `email` varchar(25) NOT NULL,
  `message` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `message`) VALUES
(2, 'Stéphanie', 's.mariejoseph@gmail.com', 'test envoie'),
(3, 'Stéphanie', 's.mariejoseph@gmail.com', 'test envoie'),
(4, 'Steph', 's.mariejoseph@gmail.com', 'TEST 234');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`,`login`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `email`) VALUES
(1, 's.mariejoseph', 'saG6Y.fLWVz/Q', 's.mariejoseph@gmail.com');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
