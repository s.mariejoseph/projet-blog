<?php

require_once __DIR__.'/../model/model.php';

class ModelArticle extends Model{
	
	public function getArticle($idArticle){
		$sql="SELECT * FROM article WHERE slug = '$idArticle'";
		return $this->queryFetchAll($sql);
	}
	public function getArticleList($from, $qty){
		$offset = $from * $qty;
		$sql="SELECT slug FROM `article` WHERE publie = 0 ORDER BY id DESC LIMIT $qty OFFSET $offset";
		return $this->queryFetchAll($sql);
	}

	public function getAllArticles(){
		$sql="SELECT * FROM article ";
		return $this->queryFetchAll($sql);
	}

	public function addArticle($titre, $slug, $image, $date, $contenu){
		$sql="INSERT INTO `article` (`titre`, `slug`, `image`, `date`, `contenu`) VALUES (:titre, :slug, :image, :date, :contenu)";
		$data = [
			"titre"    => $titre,
			"slug"     => $slug,
			"image"    => $image,
			"date" => $date,
			"contenu" => $contenu
		];
		return $this->insert($sql, $data);
	}
	
	public function updateArticle($titre, $slug, $image, $date, $contenu){
		$sql="UPDATE `article` set titre= :titre , image= :image , date= :date , contenu= :contenu  WHERE slug = :slug";
		$data = [
			"titre"    => $titre,
			"image"    => $image,
			"date" => $date,
			"contenu" => $contenu,
			"slug" => $slug
		];
		return $this->insert($sql, $data);
	}
	
	public function deleteArticle($slug){
		$sql="DELETE FROM `article`  WHERE  slug = :slug";
		$data = [
		"slug" =>  $slug
		];
		return $this->insert($sql, $data);
	}
}

?>