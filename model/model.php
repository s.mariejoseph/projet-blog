<?php

class Model{
	protected $db;
	public function __construct(){
		try{
			$this->db = new PDO('mysql:host=localhost;dbname=blog;charset=UTF8', 'root', '');
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch(Exception $e){
			echo 'Error while opening db: ' .$e->getMessage();
		}
		
	}

	protected function queryFetchAll($sql){
		$comments;
		try{
			$result = $this->db->query($sql);
			$comments = $result->fetchAll();
			$result->closeCursor();
		}
		catch(Exception $e){
			echo 'Error: ' .$e->getMessage();
		}
		return $comments;		
	}

	protected function insert($sql, $data){
		try{
			$result = $this->db->prepare($sql);
			$result->execute($data);
			$result->closeCursor();
			return true;
		}
		catch(Exception $e){
			echo 'Error: ' .$e->getMessage();
			return false;
		}
	}
}

?>