<?php

require_once __DIR__.'/../model/model.php';

class ModelUser extends Model{
	
	public function getUser($login){
		$sql="SELECT * FROM user WHERE login = '$login'";
		return $this->queryFetchAll($sql)[0];
	}
	public function getUserList($from, $qty){
		$offset = $from * $qty;
		$sql="SELECT login FROM user ORDER BY id DESC LIMIT $qty OFFSET $offset";
		return $this->queryFetchAll($sql);
	}

	public function getAllUsers(){
		$sql="SELECT * FROM user ";
		return $this->queryFetchAll($sql);
	}

	public function addUser($login, $password, $email){
		$sql="INSERT INTO user ('login', 'password', 'email') VALUES (:login, :password, :email)";
		$data = [
			"login"    => $login,
			"password" => $password,
			"email"    => $email
		];
		return $this->insert($sql, $data);
	}
	
	public function updateArticle($id, $login, $password, $email){
		$sql="UPDATE user set login= :login , password= :password , email= :email WHERE id = :id";
		$data = [
			"login"    => $login,
			"password"    => $password,
			"email" => $email,
			"id" => $id
		];
		return $this->insert($sql, $data);
	}
	
	public function deleteArticle($id){
		$sql="DELETE FROM user WHERE id = :id";
		$data = [
			"id" => $id
		];
		return $this->insert($sql, $data);
	}
}

?>