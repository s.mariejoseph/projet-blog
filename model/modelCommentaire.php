<?php

require_once __DIR__.'/../model/model.php';

class ModelCommentaire extends Model{
	
	public function getComment($slug){
		$sql="SELECT * FROM commentaire WHERE slug = '$slug'";
		return $this->queryFetchAll($sql);
	}
	
	public function getArticleComments($slugarticle,$status){
		$sql="SELECT * FROM commentaire WHERE slug_article = '$slugarticle' and status = $status";
		return $this->queryFetchAll($sql);
	}

	public function getAllComments($status){
		$sql="SELECT * FROM commentaire WHERE status = $status";
		return $this->queryFetchAll($sql);
	}

	public function addComment($title, $content, $date, $author, $slugarticle){
		$sql="INSERT INTO `commentaire`(`title`, `content`, `date`, `author`, `slug_article`, `slug`) VALUES (:title, :content, :date, :author, :slug_article, :slug)";
		$data = [
    	"title"    		=> $title,
		"content"  		=> $content,
		"date"     		=> $date,
		"author"   		=> $author,
		"slug_article"  => $slugarticle,
    	"slug"			=> uniqid(time())
		];
		return $this->insert($sql, $data);
	}

	public function updateCommentStatus($slug,$status){
		$sql="UPDATE `commentaire` set status= :status WHERE slug = :slug";
		$data = [
			"status" => $status,
			"slug" => $slug
		];
		return $this->insert($sql, $data);
	}
	
	public function removeComment($slug){
		
	}
}

?>