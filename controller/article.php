<?php

require_once __DIR__.'/../view/view.php';
require_once __DIR__.'/../model/modelArticle.php';

class Article {
public $html;
public $title;

public function __construct($slug,$wrapped = true) {
$model   = new ModelArticle();
$article = $model->getArticle($slug)[0];
$this->title = $article['titre'];

$dataView = [
"{{ title }}"   => $article['titre'],
"{{ slug }}"    => $article['slug'],
"{{ contenu }}" => $article['contenu'],
"{{ image }}"   => $article['image']
];
$template = "article.html";

If(!$wrapped){
 
$template = "resume.html";
$dataView["{{ contenu }}"] = substr ( $article['contenu'] , 0 , 250 )." [...]";

}

$vue = new View( $dataView, $template );
$this->html = $vue->html;

}
}
?>

