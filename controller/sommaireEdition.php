<?php

require_once __DIR__.'/../view/view.php';
require_once __DIR__.'/../model/modelArticle.php';

class SommaireEdition {
	public $articleList = [];
	public $html = "";
	function __construct(){
		$model = new ModelArticle();
		$this->articleList = $model->getAllArticles();
		$article;
		
		$articlelisthtml = "";
		
		foreach ($this->articleList as $key => $value) {
			
			$vue = new View(
				[
					"{{ titre }}"=> $value['titre'],
					"{{ slug }}"=> $value['slug']
				],
				"sommaireEditionArticle.html"
			);
			
			$articlelisthtml .= $vue->html;
		}
		
		$vue = new View(
			[
				"{{ articles }}"=> $articlelisthtml
			],
			"listeSommaireArticle.html"
		);
		$this->html = $vue->html;
	}
}
?>