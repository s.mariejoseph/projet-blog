<?php

require_once __DIR__.'/../controller/listArticle.php';
require_once __DIR__.'/../controller/article.php';
require_once __DIR__.'/../view/view.php';

require_once __DIR__.'/../controller/listComment.php';
require_once __DIR__.'/../controller/comment.php';
require_once __DIR__.'/../controller/editComment.php';
require_once __DIR__.'/../controller/email.php';

class Front{
	private $uri;

	public function __construct($uri){
		global $secure;
		$message =""; 
		if ($secure->post["author"]!= NULL && $secure->post["email"]!= NULL && $secure->post["content"]!= NULL){ 
			$message = new Email();  //ack
		} 
		
		$this->uri = $uri;

		$this->route($this->uri);
		
		$vue = new View(
			[
				"{{ content }}"=> $this->content, 
				"{{ title }}"=> $this->title,
				"{{ message }}"=> $message
		
			],
			"front-read.html"
		);
		
		$this->html= $vue->html;
	}
	
	private function route($uri){
		switch ($uri[0]) {
			case 'bio':
		        $this->bio();
		    break;
			case 'accueil':
				$this->articles();
			break;
			case 'articles':
				$this->articles();
			break;
			case 'article':
				$this->article();
			break;
			case 'comments':
				$this->comments();
			break;
			case 'comment':
				$this->comment();
			break;
			case 'new-article':
				$this->newarticle();
			break;
			default:
				$this->articles();
			break;
		}
	}
	
	private function articles(){
		$this->listArticles(0,30);
	}

	private function article(){
		$article = new Article($this->uri[1]);
		
		$this->title = $article->title;
		$this->content = $article->html;
		
		$comment = new Comment();
		$comment->getArticleComments($this->uri[1]);
		$this->content .= $comment->html;
		
		$editionComment=new EditionComment($this->uri[1]);
		$this->content.= $editionComment-> html;
		
	}
	
	private function comments(){
		$comment = new Comment();
		$comment-> getArticleComments($this->uri[1]);
		$this-> title = "comments";
		$this-> content = $comment-> html;
		
	}
	
	private function comment(){
		$comment = new Comment();
		$comment -> getComment($this->uri[1]);
		$this -> title = "comment";
		$this -> content = $comment -> html;
	}

	private function listArticles($debut, $quantite){
		$articles = new ListArticle($debut, $quantite);
		$this->content= $articles->html;
		$this->title="Accueil"; 
		
	}
	
	private function newarticle(){
		$newarticle=new Edition($_POST);
	}
	
	private function bio(){
		$this->content= "biographie...";
		$this->title="Biographie"; 
	}
}

?>