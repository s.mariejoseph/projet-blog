<?php

class Endsession {

	public function __construct() {
		foreach ($_SESSION as $key=>$value){
			if (isset($GLOBALS[$key])) unset($GLOBALS[$key]);
		}
		session_destroy();
		header('Location: ../accueil',false); // redirection si OK
		exit();
	}
}

?>

