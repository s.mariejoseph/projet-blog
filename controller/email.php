<?php

require_once __DIR__.'/../view/view.php';
require_once __DIR__.'/../model/modelEmail.php';

class Email{
	private $ack;
	
	public function __construct()
	{
		global $secure;
		global $config;
		
	   
		$to = $config["email"]; 
		$subject = "Vous avez un nouveau message de ".$secure->post['author']; 
		$message = $secure->post["content"]; 
		$headers = array( 'From' => $config["emailServer"], 'Reply-To' => $secure->post["email"], 'X-Mailer' => 'PHP/' . phpversion() );
		
		//try&catch mail pour qu'on ait un message d'envoi dans le try = mail et catch = pas envoyé + rajouter une fonction
		
		try {
			mail($to, $subject, $message, $headers);
		
			$this->ack="Votre message à bien été envoyé et sera traité sous peu de temps";
			
		} catch (Exception $e) {
			//echo 'Error : ',  $e->getMessage(), "\n";
			$this->ack="Une erreur est survenue lors de l'envoie du message";
		}
		
	}
	public function __toString() {
		return $this->ack; 
	}
}