<?php

require_once __DIR__.'/../view/view.php';
require_once __DIR__.'/../model/modelArticle.php';

class Edition {
	public $html;

	public function __construct($slugInUrl) {
		global $secure;
		
		$slugpost=$slugInUrl;
		$isset = $secure->post["title"] !== null && $secure->post["slug"] !== null && $secure->post["content"] !== null && $secure->post["image"] !== null;
		
		if ($secure->post["status"] !== null) {
			$this->suppress($slugpost);
		}
		else if ($isset) {
			$slugpost=$_POST["slug"];
			$this->insert($secure->post,$slugpost);
		}
		else{
			$this->edit($slugpost);
		}
	}
	
	
	private function edit($slugpost){
		
		$title="";
		$slug="";
		$content="";
		$image="";
		
		if(strlen($slugpost)>0){
			$model   = new ModelArticle();
			$article = $model->getArticle($slugpost)[0];
			
			$title=$article['titre'];
			$slug=$article['slug'];
			$content=$article['contenu'];
			$image=$article['image'];
		}

		$dataView = [
			"{{ title }}"   => $title,
			"{{ slug }}"    => $slug,
			"{{ content }}" => $content,
			"{{ image }}"   => $image
		];
		$template = "editionArticle.html";

		$vue = new View( $dataView, $template );
		$this->html = $vue->html;
	}
	
	private function suppress($slugpost){
		$model   = new ModelArticle();
		$model->deleteArticle($slugpost);
		header('Location: ../admin/home-edit/',false); // redirection si OK
		exit();
	}

	
	private function insert($post,$slugpost){
		$title = htmlspecialchars($_POST["title"]);
		$slug = $slugpost;
		if(isset($_POST["slug"])){
			$slug = htmlspecialchars(str_replace(" ","",$_POST["slug"]));
		}
		$image = htmlspecialchars($_POST["image"]);
		$date = date("Y-m-d H:i:s");
		$content = $_POST["content"];
		
		$model   = new ModelArticle();
		
		$nbArticle = count($model->getArticle($slugpost));
		if($nbArticle > 0){
			$model->updateArticle($title, $slug, $image, $date, $content);
		}
		else{
			$model->addArticle($title, $slug, $image, $date, $content);
		}
		header('Location: ../admin/home-edit/',false); // redirection si OK
		exit();
	}

}
?>

