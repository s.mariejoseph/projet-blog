<?php

require_once __DIR__.'/../view/view.php';
require_once __DIR__.'/../model/modelArticle.php';

class Sommaire {
	public $articleList = [];
	public $html = "";
	function __construct(){
		
		$vue = new View(
				[],
				"sommaireAdmin.html"
			);
			
		$this->html = $vue->html;
	}
}
?>