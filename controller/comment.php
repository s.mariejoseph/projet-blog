<?php

require_once __DIR__.'/../view/view.php';
require_once __DIR__.'/../model/modelCommentaire.php';

class Comment{
	
	private $model;
	public $html;
	
	
	public function __construct()
	{
		global $secure;
		$this->model = new ModelCommentaire();
		$this->html = "";
		if(isset($secure->post["title"]) && isset($secure->post["author"]) && isset($secure->post["content"])) {
			$this->addComment();
		}
		if(isset($secure->post["status"]) && isset($secure->post["slugComment"])) {
			$this->updateCommentStatus();
		}
	}
	
	
		
	private function addComment(){
		global $secure;
		$this->model->addComment(
			$secure->post["title"], 
			$secure->post["content"], 
			date("Y-m-d H:i:s"),
			$secure->post["author"], 
			$secure->post["slug"]
			
		);
	}
	
	private function updateCommentStatus(){
		global $secure;
		$this->model->updateCommentStatus($secure->post["slugComment"],$secure->post["status"]);
	}
	
	public function getComment($slug){
		$comment = $this->model->getComment($slug)[0];
		$commenthtml = $this->buildCommentHtml($comment,"comment.html");
		$this->html = $this->wrap($commenthtml);
	}
	
	public function getArticleComments($slugarticle){
		$commentlisthtml = "";
		
		$template="comment.html";
		$commentList = $this->model->getArticleComments($slugarticle,1);
		
		foreach ($commentList as $key => $value) {
			$commenthtml = $this->buildCommentHtml($value,$template);
			$commentlisthtml .= $commenthtml;
		}
		
		$template="moderatedComment.html";
		$commentList = $this->model->getArticleComments($slugarticle,3);
		
		foreach ($commentList as $key => $value) {
			$commenthtml = $this->buildCommentHtml($value,$template);
			$commentlisthtml .= $commenthtml;
		}
		
		$this->html = $this->wrap($commentlisthtml);
	}
	
	public function getAllComments($action){
		$commentList;
		
		if($action=="validation"){
			$template="commentValidation.html";
			$commentList = $this->model->getAllComments(0);
		}
		else if($action=="moderation"){
			$template="commentModeration.html";
			$commentList = $this->model->getAllComments(2);
		}
		$commentlisthtml = "";
		foreach ($commentList as $key => $value) {
			$commenthtml = $this->buildCommentHtml($value,$template);
			$commentlisthtml .= $commenthtml;
		}
		$this->html = $this->wrap($commentlisthtml);
	}
	
	
	
	private function buildCommentHtml($comment,$template){
		$vue = new View(
			[
				"{{ titre }}"=> $comment['title'],
				"{{ contenu }}"=> $comment['content'],
				"{{ auteur }}"=> $comment['author'],
				"{{ slugComment }}"=> $comment['slug'],
				"{{ slug }}"=> $comment['slug_article'],
			],
			$template
		);
		return $vue->html;
	}
	
	private function wrap($html){
	//die(var_dump($html));	
		$wrapvue = new View(
				[
					"{{ comment }}"=> $html
				],
				"listeComment.html"
		);
		
		return $wrapvue->html;
	}
	
}