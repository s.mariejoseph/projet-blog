<?php

require_once __DIR__.'/../view/view.php';
require_once __DIR__.'/../model/modelCommentaire.php';

class EditionComment {
	public $html;

	public function __construct($slug) {
		$this->edit($slug);
	}
	
	
	private function edit($slug){
		
		$title="";
		$content="";
		$author="";
		
		$dataView = [
			"{{ title }}"   => $title,
			"{{ slug }}"    => $slug,
			"{{ content }}" => $content,
			"{{ author }}"  => $author
		];
		$template = "editionCommentaire.html";

		$vue = new View( $dataView, $template );
		$this->html = $vue->html;
	}
	
}
?>

