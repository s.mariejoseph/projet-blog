<?php

class Security{

	public $post;
	public $get;
	public $uri;
	public function __construct($arguments){
		
		if(isset($arguments["POST"])){
			$this->post = filter_input_array(INPUT_POST,$arguments["POST"]);
		}
		if(isset($arguments["GET"])){
			$this->get = filter_input_array(INPUT_GET,$arguments["GET"]);
		}	
		$this->uri = filter_var($_SERVER['REQUEST_URI'], FILTER_SANITIZE_URL, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
	}
	public function checkLogged(){
		
		if(!isset($_SESSION['login'])){
			header('Location: ../login-failed',false);
			die();
			
		}
		
	}
}

	
?>