<?php

session_start();

require_once __DIR__.'/../view/view.php';
require_once __DIR__.'/../model/modelUser.php';

class Authentication {
	public $html;

	public function __construct() {
		global $secure;
		$isset = $secure->post["login"] !== null && $secure->post["password"] !== null;
		
		if ($isset) {
			$this->check($secure->post);
		}
		else{
			$this->authenticate();
		}
		
	}
	
	private function authenticate(){
		
		/*foreach ($_SESSION as $key=>$value){
			echo $key."<br>";
			if (isset($GLOBALS[$key])) unset($GLOBALS[$key]);
		}*/
		//die();
		
	
		$dataView = [
			
		];
		$template = "authentification.html";

		$vue = new View( $dataView, $template );
		$this->html = $vue->html;
	}

	
	private function check($user){
		
		
		$login = $user["login"];
		$password = $user["password"];
		
		$model   = new ModelUser();
		$user = $model->getUser($login);
		if($user["password"]===crypt($password,"salt-18795642")){
	
			$_SESSION["user_role"] = "admin";
			$_SESSION["user_login"] = $login;
			
			//Generate a random string.
			$token = openssl_random_pseudo_bytes(16);
 
			//Convert the binary data into hexadecimal representation.
			$token = bin2hex($token);
			
			
			$_SESSION['token'] = $token;
			
			setcookie('token', $token, time() + 2*3600 , "/");
			
			
			header('Location: ../admin/home-edit/',false); // redirection si OK
			exit();
		}
		else {
			header('Location: ../admin/login-failed',false); // redirection si utilisateur non reconnu
		}
		
	}

}
?>

