<?php

require_once __DIR__.'/../controller/edition.php';
require_once __DIR__.'/../controller/authentification.php';
require_once __DIR__.'/../controller/endsession.php';
require_once __DIR__.'/../view/view.php';
require_once __DIR__.'/../controller/sommaireEdition.php';
require_once __DIR__.'/../controller/sommaire.php';

class Admin{
	private $uri;
	private $content;

	public function __construct($uri){
		
		
		$this->uri = $uri;

		$this->route($this->uri);
		
		$vue = new View(
			[
				"{{ content }}"=> $this->content, 
		
			],
			"admin.html"
		);
		
		$this->html= $vue->html;
	}
	
	private function route($uri){ //edit-article
	
		//var_dump($uri);
	
		// $slugInUrl="";
		// switch ($uri[0]) {
		// 	case 'edit-article':
		// 		if(count($uri) > 0){
		// 			$slugInUrl=$uri[1];
		// 		}
		// 		$this->newarticle($slugInUrl);
		// 	break;
		// 	case 'login':
		// 		$this->login();
		// 	break;
		// 	case 'login-failed':
		// 		$this->loginFailed();
		// 	break;
		// 	default:
		// 		$this->newarticle($slugInUrl);
		// 	break;
		// }
		$todo = $uri[0]; //edit-article
		
		if (!isset($_SESSION["user_login"])||($_COOKIE["token"]!=$_SESSION["token"])) $todo="login";
		if (strrpos($todo, "-")){
			$todo    = explode("-", $todo);
			$todo[1] = strtoupper(substr($todo[1], 0,1)).substr($todo[1], 1);
			$todo    = implode("", $todo);
		}
		//editArticle
		if (!method_exists($this, $todo)) $todo = "homeEdit";
		$this->$todo();
		// $this->editArticle()

	}
	
	private function homeEdit(){
		$sommaire=new Sommaire();
		$this->content=$sommaire -> html;
		$this->title="Menu Admin"; 
	}
	
	private function homeEditArticle(){
		$sommaire=new SommaireEdition();
		$this->content=$sommaire -> html;
		$this->title="Sommaire Edition article"; 
	}

	private function editArticle(){
		$slugInUrl="";
		
			if(count($this->uri) > 0){
				$slugInUrl=$this->uri[1];
				}
				
		$newarticle=new Edition($slugInUrl);
		$this->content=$newarticle -> html;
		$this->title="Edition article"; 
	}
	
	private function editComments(){
		$this -> title = "comments";
		$comment = new Comment();
		$comment -> getAllComments("validation");
		$validation = $comment -> html;
		$comment -> getAllComments("moderation");
		$moderation = $comment -> html;
		
		$vue = new View(
			[
				"{{ moderation }}"=> $moderation, 
				"{{ validation }}"=> $validation
		
			],
			"ModerationCommentaire.html"
		);
		$this -> content = $vue -> html;
	}
	
	private function login(){
		$authentication=new Authentication();
		$this->content=$authentication -> html;
		$this->title="Login"; 
	}
	
	private function logout(){
		new Endsession();
	}
	
	private function loginFailed(){
		$this->content="invalid login or password !!!";
		$this->title="Login failed"; 
	}
	
}

?>