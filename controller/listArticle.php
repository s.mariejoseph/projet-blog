<?php 

require_once __DIR__.'/../model/modelArticle.php';
require_once __DIR__.'/../controller/article.php';
require_once __DIR__.'/../view/view.php';

class ListArticle{
	public $articleList = [];
	public $html = "";
	function __construct($debut, $quantite){
		$model = new ModelArticle();
		$this->articleList = $model->getArticleList($debut, $quantite);
		$article;
		$articlelisthtml = "";
		foreach ($this->articleList as $key => $value) {
			$article = new Article($value['slug'], false);
			$articlelisthtml .= $article->html;
		}
		
		$vue = new View(
			[
				"{{ article }}"=> $articlelisthtml
			],
			"listeArticle.html"
		);
		$this->html = $vue->html;
	}
}