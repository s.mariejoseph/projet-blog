<?php


class View
{
  public $html = "";
  
  function __construct($data, $template)
  {
	global $config;	
	
	$data["{{ path }}"]=$config["path"];
    $this->data = $data;
    $this->template = "template/".$template;
    if (isset($data[0])) $this->makeLoopHtml();
    else $this->makeHtml();
  }

  private function makeLoopHtml(){
    $size = count($this->data);
    for ($i=0; $i<$size; $i++){
      $this->html .= $this->mergeWithTemplate($this->data[$i]);
    }
  }

  private function makeHtml(){
    $this->html = $this->mergeWithTemplate($this->data);
  }

  private function mergeWithTemplate($args){
	$content=file_get_contents($this->template);
    $res= str_replace(
      array_keys($args),
      $args,
      $content
    );
	return $res;
  }
}

?>